import argparse
import glob
import json
import os
import os.path as os_path
import shutil

import cv2
import numpy as np
#ile = open('label_1.json')
src_dir = "./"
binary_dst_dir = "./training/gt_binary_image"
instance_dst_dir = "./training/gt_instance_image"
ori_dst_dir = "./training/gt_image"
#print('*\n*****************************************\n*')
#print(file)
#print(json.load(file)["lanes"])
file_path = "label.json"
# el json tiene que ser de la forma.
"""
{
  "lanes": [
    [647, 253, -2, -2, -2, -2, -2, -2],  son los valores en el eje x
    [-2, -2, 656, 528, -2, -2, -2, -2],  cada lista corresponde a una linea
    [-2, -2, -2, -2, 666, 870, -2, -2],  los valores -2 corresponde a que no hay valores para ese punto
    [-2, -2, -2, -2, -2, -2, 679, 1160]  
    ],
  "h_samples": [246, 719, 248, 719, 248, 719, 247, 719],  son todos los valores asociados al eje y
  "raw_file": "./data/training_data_example/gt_image/0006.png"
}
"""
with open(file_path) as file:
    info_dict = json.loads(file.read())
    print(enumerate(file.read()))
    for i in range(1):
            #info_dict = json.loads(line.reads())
            print("EEEEEEEEEEEEEEEEEEEE")
            image_dir = os_path.split(info_dict['raw_file'])[0]
            # image_dir = 'c://path/to/folder'
            image_dir_split = image_dir.split('/')[1:]
            # image_dir_split = ['path', 'to', 'folder']
            image_dir_split.append(os_path.split(info_dict['raw_file'])[1])
            # image_dir_split = ['path', 'to', 'folder', 'img.jpg']
            image_name = '_'.join(image_dir_split)
            # image_name = 'path_to_folder_img.jpg'
            image_path = os_path.join(src_dir, info_dict['raw_file'])
            # image_path = Path('src_dir/path/to/folder)
            assert os_path.exists(image_path), '{:s} not exist'.format(image_path)

            h_samples = info_dict['h_samples']

            lanes = info_dict['lanes']

            image_name_new = '{:s}.png'.format('{:d}'.format(i + 6).zfill(4))
            # image_name_new = '0055.png'

            src_image = cv2.imread(image_path, cv2.IMREAD_COLOR)
            dst_binary_image = np.zeros([src_image.shape[0], src_image.shape[1]], np.uint8)
            dst_instance_image = np.zeros([src_image.shape[0], src_image.shape[1]], np.uint8)

            for lane_index, lane in enumerate(lanes):
                assert len(h_samples) == len(lane)
                lane_x = []
                lane_y = []
                for index in range(len(lane)):
                    if lane[index] == -2:
                        continue
                    else:
                        ptx = lane[index]
                        pty = h_samples[index]
                        lane_x.append(ptx)
                        lane_y.append(pty)
                if not lane_x:
                    continue
                lane_pts = np.vstack((lane_x, lane_y)).transpose()
                # lane_pts = [[1,5],[2],[5]]
                lane_pts = np.array([lane_pts], np.int64)

                cv2.polylines(dst_binary_image, lane_pts, isClosed=False,
                              color=255, thickness=5)
                cv2.polylines(dst_instance_image, lane_pts, isClosed=False,
                              color=lane_index * 50 + 20, thickness=5)

            dst_binary_image_path = os_path.join(binary_dst_dir, image_name_new)
            dst_instance_image_path = os_path.join(instance_dst_dir, image_name_new)
            dst_rgb_image_path = os_path.join(ori_dst_dir, image_name_new)

            cv2.imwrite(dst_binary_image_path, dst_binary_image)
            cv2.imwrite(dst_instance_image_path, dst_instance_image)
            cv2.imwrite(dst_rgb_image_path, src_image)

            print('Process {:s} success'.format(image_name))
#print(data)

                               #info_dict = json.loads(line)
# with open("label.json", errors='ignore') as json_data:
#      data = json.loads(json_data, strict=False)
# print(data)     