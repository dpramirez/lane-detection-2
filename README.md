# Requirements
 

`pip install -r requirements.txt`

# Consider

have vgg16 in the ./data folder, you can download it from the following link:

[https://drive.google.com/file/d/1UXFUMQikc9YUKmSFBDh3aOEHBANWTwyQ/view?usp=sharing](url)


# Data set used

The data set you occupy is your own and they are in the folder ./test_image

# Json 

label.json contains the images I want to test, they have a certain format.

be careful that in this case it only contains one file you can see the complete label.json here:

[https://drive.google.com/file/d/155UYAVknl3ayYmd0yR6OUy0tggQCZ8nC/view?usp=sharing](url)

# Prepare the data

The first thing we have to do is choose, how much data do we want to train, 
that's why we check the label.json file and each line corresponds to an image. 
After selecting the images we proceed to create the data for the training with 
the command:

`python .\generate_tusimple_dataset.py --src_dir ./`

With this, two folders will be created, one Training and the other Testing.

Copy the Training content into the ./data/training_data_example folder

With the copied data we can proceed the data to fit the model, without first configuring the ./config/global_config.py file where the number of elements to train must be set.

We execute the command to generate the tfrecord:


`python .\lanenet_data_feed_pipline.py --dataset_dir ./data/training_data_example --tfrecords_dir ./data/training_data_example/tfrecords`

# Training

With the following command you can, you can train the model:

`python train_lanenet.py --net vgg --dataset_dir ./data/training_data_example -m 0`

# Credits

https://github.com/MaybeShewill-CV/lanenet-lane-detection
